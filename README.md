# Source code for the article Chevalier et al. (2020) Am. Nat.

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/) [![DOI:10.1086/711417](https://zenodo.org/badge/DOI/10.1086/711417.svg)](https://doi.org/10.1086/711417)

This repository contains the R source code of the model described in Chevalier L., Labonne J., Galipaud M., Dechaume-Moncharmont F.-X. (2020) Fluctuating dynamics of mate availability promote the evolution of flexible choosiness in both sexes. *American Naturalist* 196:730-742 https://doi.org/10.1086/711417. 

**Abstract**. The evolution of choosiness has a strong effect on sexual selection, as it promotes variance in mating success among individuals. The context in which choosiness is expressed, and therefore the associated gain and cost, is highly variable. An overlooked mechanism by current models is the rapid fluctuations in the availability and quality of partners, which generates a dynamic mating market to which each individual must optimally respond. We argue that the rapid fluctuations of the mating market are central to the evolution of optimal choosiness. Using a dynamic game approach, we investigate this hypothesis for various mating systems (characterized by different adult sex ratio and latency period combinations), allowing feedback between the choosiness and partner availability throughout a breeding season while taking into account the fine variation in individual quality. Our results indicate that quality-dependent and flexible choosiness usually evolve in both sexes for various mating systems and that a significant amount of variance in choosiness is observed, especially in males, even when courtship is costly. Accounting for the fluctuating dynamics of the mating market therefore allows envisioning a much wider range of choosiness variation in natural populations and may explain a number of recent empirical results regarding choosiness in the less common sex or its variance within sexes.

**Keywords** : game theory, dynamic programming, sampling strategy, mate choice

	Project
	  ├── README
	  ├── DP_OptimalChoosiness.R
	  ├── Shiny App code for Choosiness.tar
	  └── Shiny App code for Partner availability.tar

It can be run in [R software](https://www.r-project.org/) as it is. It simulates the dynamics of optimal choosiness in a game where both sexes can select their partner(s) in a finite time window, with respect to partner quality. The second item is R code for Shiny Application, that can be run directly in R software. It allows exploring the results of the model in terms of partner availability, under different simulation scenarios. The third item is also R code for Shiny Application, that can be run directly in R software. It allows exploring the results of the model in terms of optimal choosiness, under different simulation scenarios. These two last items are provided as compressed .tar files.
